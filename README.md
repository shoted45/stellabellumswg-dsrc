# Stella Bellum Star Wars Galaxies DSRC/Script Repo
## Pull requests are welcome and encouraged
#### Please credit Stella Bellum (notably Cekis) if you use any of the work here

See [Stella Bellum](https://forums.stellabellum.net/) for discussion and information. For a good FAQ/Help site, please see [SWG Source](https://www.swgsource.com/forums/)

